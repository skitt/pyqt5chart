Source: pyqt5chart
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Stephen Kitt <skitt@debian.org>
Build-Depends: debhelper (>= 10.3),
               dh-python,
               libqt5charts5-dev,
               pyqt5-dev,
               python3-all-dbg,
               python3-all-dev,
               python3-pyqt5,
               python3-pyqt5-dbg,
               python3-sip-dbg,
               python3-sip-dev,
               qttools5-dev
Standards-Version: 4.1.4
Homepage: http://www.riverbankcomputing.co.uk/software/pyqtchart/
Vcs-Git: https://salsa.debian.org/python-team/modules/pyqt5chart.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/pyqt5chart

Package: pyqt5chart-dev
Architecture: all
Depends: python3-sip-dev, ${misc:Depends}
Description: Development files for PyQtCharts
 PyQtCharts exposes the QtCharts toolkit in Python.
 .
 This package contains the source SIP files from which the Python
 bindings for QtCharts are created.

Package: python3-pyqt5.qtchart
Architecture: any
Depends: python3-pyqt5,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: Python 3 bindings for Qt5's Charts module
 The Charts module of PyQt5 provides widgets and utility classes
 for chart rendering in a PyQt5 application.
 .
 This package contains the Python 3 version of this module.

Package: python3-pyqt5.qtchart-dbg
Section: debug
Architecture: any
Depends: python3-dbg,
         python3-pyqt5-dbg,
         python3-pyqt5.qtchart (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Python 3 bindings for Qt5's Charts module (debug extension)
 The Charts module of PyQt5 provides widgets and utility classes
 for chart rendering in a PyQt5 application.
 .
 This package contains the extension built for the Python 3 debug interpreter.
